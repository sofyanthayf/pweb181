<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

  public function __construct() {
      parent::__construct();

      $this->load->model('mahasiswa_model');
  }

  public function listmhs( $tahun )
  {

    $daftar_mhs = $this->mahasiswa_model->daftar_mhs( $tahun );

    $viewdata['daftarmahasiswa'] = $daftar_mhs;

    $this->load->view('layout/header');
    $this->load->view('layout/nav');
    $this->load->view( 'mahasiswa/daftar_mahasiswa', $viewdata );
    $this->load->view('layout/footer');


  }




}

?>
