<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

// class Mysiska_api extends CI_Controller {
class Api_mahasiswa extends REST_Controller {

  public function __construct($config = 'rest') {

    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    parent::__construct();

    $this->load->model('mahasiswa_model');

  }

  public function listmahasiswa_get()
  {

    $tahun = $this->get( 'angkatan' );

    $listmh = $this->mahasiswa_model->daftar_mhs( $tahun );

    $res = array();
    $res['angkatan'] = $tahun ;
    $res['jumlah'] = count( $listmh );
    $res['list_mahasiswa'] = $listmh;

    if( $res['jumlah'] > 0 ){
      $this->response( [ 'mahasiswa' => $res ] , 200  );
    } else {
      $this->response( 'no data' , 204  );
    }

  }




}

?>
