<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Santai extends CI_Controller {

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('layout/nav');

		$this->load->view('santai_yuk');

		$this->load->view('layout/footer');
	}

	public function waktuhabis()
	{
		$this->load->view('layout/header');
		$this->load->view('layout/nav');

		$this->load->view('santai_selesai');

		$this->load->view('layout/footer');

	}

}
