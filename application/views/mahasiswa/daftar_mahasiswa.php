<section class="content-section bg-light" id="about">
  <div class="container text-center">
    <div class="row">
      <div class="col-lg-10 mx-auto">

 <table class="table table-striped table-border">
   <thead>
     <tr>
       <td>NIM</td>
       <td>Nama</td>
       <td>Email</td>
     </tr>
   </thead>
   <tbody>
     
     <?php
        foreach ($daftarmahasiswa as $mhs ) {
      ?>

      <tr>
        <td><?= $mhs['nimhs'] ?></td>
        <td><?= $mhs['namamhs'] ?></td>
        <td><?= $mhs['email'] ?></td>
      </tr>

      <?php
        }
      ?>

   </tbody>
 </table>

</div>
</div>
</div>
</section>
